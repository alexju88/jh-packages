# 用Helm工具在K8S中安装极狐Gitlab

## 1. 概述

在某些应用场景下，我们需要将极狐Gitlab部署到k8s容器服务平台上。

将Gitlab部署到k8s上有如下优点：

- 便于管理集群，容器资源可快速创建、销毁和扩容
- 可以快速拉起或删除极狐Gitlab相关服务，也可以灵活地横向扩展
- 可以集中分配和管理硬件资源，减少硬件资源的使用量，并更加易于管理
- 可以实现高可用，减少部署高可用集群的操作复杂度

本文档使用GitLab官方推荐的Helm chart方式在k8s集群中部署极狐GitLab，**Helm**是k8s的包管理工具，**Chart**是一个 Helm 包，其中包含了运行一个应用所需要的镜像、依赖和资源定义等，详情请参考：[GitLab installed on Kubernetes（K8S）](https://docs.gitlab.com/charts/)



## 1.1 安装的前置条件

推荐使用root用户安装部署，不会出现权限问题

本文使用的操作系统是Ubuntu 18.04

- 安装docker

  ```bash
  apt-get install docker
  ```

- 部署k8s集群

  这里安装k3d工具并快速拉起k8s集群，尔后手动配置kubectl以访问k8s集群

- 可访问的域名，该域名用于访问通过chart部署的GitLab实例，例如`example.com`

  如果您是在本地部署并使用，仅需要手动指定域名即可，如果需要通过外网访问，需要通过DNS服务器指定域名并进行关联

- 硬件资源与配置要求：[安装前置条件](https://docs.gitlab.com/charts/installation/index.html)

- 在通过Helm Chart部署k8s集群时可以通过Chart的configure指定部署Pod的数量，实际所需要的资源也会因部署的规模而变化（比如硬盘空间），需要根据实际使用情况分配使用资源。



## 2. 需要部署的相关组件

Gitlab的应用栈涉及多种组件，通过Helm chart方法可以将Gitlab的组件以pod的方式自动部署到k8s集群中

[使用Helm chart部署的相关组件](https://docs.gitlab.com/charts/#introduction)

[k8s GitLab部署架构](https://docs.gitlab.com/charts/architecture/architecture.html)

## 3. 使用Helm Chart快速部署GitLab

参考文档：[Install the Cloud Native GitLab chart onto Kubernetes](https://docs.gitlab.com/charts/quickstart/index.html)

### 3.1 概述

Helm是k8s的包管理工具，类似Linux系统常用的 apt、yum等包管理工具。

使用helm可以简化k8s应用部署

### 3.2 基本概念

- **Chart**：一个 Helm 包，其中包含了运行一个应用所需要的镜像、依赖和资源定义等，还可能包含 Kubernetes 集群中的服务定义，类似 Homebrew 中的 formula、APT 的 dpkg 或者 Yum 的 rpm 文件。
- **Release**：在 Kubernetes 集群上运行的 Chart 的一个实例。在同一个集群上，一个 Chart 可以安装很多次。每次安装都会创建一个新的 release。例如一个 MySQL  Chart，如果想在服务器上运行两个数据库，就可以把这个 Chart 安装两次。每次安装都会生成自己的 Release，会有自己的  Release 名称。
- **Repository**：用于发布和存储 Chart 的存储库。

![team view](img/helm_chart_k8s.png)

### 3.3 基于Helm部署Gitlab

#### 3.3.1 Helm安装使用

Helm支持多种安装方式，详情请参考：[Helm install](https://helm.sh/docs/intro/install/)

```bash
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
$ chmod 700 get_helm.sh
$ ./get_helm.sh
```

**注意：**获取helm安装脚本和安装helm都需要通过代理访问外网，安装完成后需要关闭代理服务。

Helm的基本使用方式和介绍：[Helm 快速向导](https://helm.sh/docs/intro/quickstart/)

#### 3.3.2 添加Gitlab的repo并安装部署Gitlab

将gitlab repo添加到helm中

```bash
helm repo add gitlab https://charts.gitlab.io/
```



查看是否添加成功

```bash
owen@master-node:~$ helm search repo gitlab

NAME                         	CHART VERSION	APP VERSION	DESCRIPTION                                       
gitlab/gitlab                	4.9.1        	13.9.1     	Web-based Git-repository manager with wiki and ...
gitlab/gitlab-omnibus        	0.1.37       	           	GitLab Omnibus all-in-one bundle                  
gitlab/gitlab-runner         	0.26.0       	13.9.0     	GitLab Runner                                     
gitlab/kubernetes-gitlab-demo	0.1.29       	           	GitLab running on Kubernetes suitable for demos   
stable/gitlab-ce             	0.2.3        	9.4.1      	GitLab Community Edition                          
stable/gitlab-ee             	0.2.3        	9.4.1      	GitLab Enterprise Edition                         
gitlab/apparmor              	0.2.0        	0.1.0      	AppArmor profile loader for Kubernetes            
gitlab/auto-deploy-app       	0.8.1        	           	GitLab's Auto-deploy Helm Chart                   
gitlab/elastic-stack         	3.0.0        	7.6.2      	A Helm chart for Elastic Stack                    
gitlab/fluentd-elasticsearch 	6.2.6        	2.8.0      	A Fluentd Helm chart for Kubernetes with Elasti...
gitlab/knative               	0.10.0       	0.9.0      	A Helm chart for Knative                          
gitlab/plantuml              	0.1.16       	1.0        	PlantUML server                                   

```



### 3.3.3 安装kubectl

参考[安装kubectl](https://kubernetes.io/docs/tasks/tools/)选择合适的方式安装kubectl



### 3.3.4 安装gitlab/gitlab chart

首先，创建gitlab-values.yaml文件，制定极狐Gitlab使用的镜像地址，并设定pv的配置信息

```bash
# vim gitlab-values.yaml
global:
  kubectl:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/kubectl
      tag: 1.14.10
  certificates:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/alpine-certificates

nginx-ingress:
  controller: 
    image:
      repository: docker.io/siriuszg/nginx-ingress-controller
      digest: sha256:54757f16ed56d1ec048fe989523734528e1e75248aa55195ad8e6efb244b1a0c
  defaultBackend: 
    image:
      repository: docker.io/v5cn/defaultbackend-amd64

registry:
  image:
    repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-container-registry
gitlab:
  gitlab-exporter:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-exporter
  gitlab-shell:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-shell
  gitaly:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitaly
    persistence:
      size: 30Gi
  gitlab-pages:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-pages
  mailroom:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-mailroom
  migrations:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-task-runner-jh
  sidekiq:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-sidekiq-jh
  webservice:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-webservice-jh
    workhorse:
      image: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-workhorse-jh
  task-runner:
    image:
      repository: gitlab-jh.tencentcloudcr.com/cng-images/gitlab-task-runner-jh
postgresql:
  persistence:
    size: 10Gi
redis:
  master:
    persistence:
      size: 10Gi
prometheus:
  server:
    persistentVolume:
      size: 10Gi
```



这里的`--set`选项是chart的configure，Helm通过chart+configure的方式实现部署，configure可以视为部署chart时需要定义的变量，这里也可以使用`helm install gitlab gitlab/gitlab -f xxx.yaml`的方式批量配置configure，详细内容请参考：[Gitlab chart](https://gitlab.com/gitlab-org/charts/gitlab)

在安装极狐Gitlab时，需要使用`--values`指定极狐Gitlab使用的镜像和创建pv的配置信息

假如这里使用`example.com`作为域名，部署成功后便可通过`https://gitlab.example.com`访问极狐Gitlab实例

```bash
helm install gitlab gitlab/gitlab \
  --set global.hosts.domain=example.com \
  --set certmanager-issuer.email=xouyang@gitlab.cn
  
helm install gitlab gitlab/gitlab --timeout 600s \
  --set global.hosts.domain=mygitlab.xouyang.xyz \
  --set certmanager-issuer.email=xouyang@jihulab.com  \
  --values gitlab-values.yaml 
```



成功安装chart后可以运行`helm ls`查看已安装的chart列表，Helm需要依赖kubectl命令访问k8s

```bash
helm ls
NAME  	NAMESPACE	REVISION	UPDATED                               	STATUS  	CHART       	VERSION
gitlab	default  	1       	2021-03-02 14:38:12.87238336 +0800 CST	deployed	gitlab-4.9.1	13.9.1     
```



在k8s节点上，可以查看通过kubectl查看chart自动部署的pod信息，每个gitlab组件都是运行在pod中的服务

```bash
root@owen-KVM:~# kubectl get pods 
NAME                                                   READY   STATUS      RESTARTS   AGE
svclb-gitlab-nginx-ingress-controller-62zpd            0/3     Pending     0          23h
svclb-gitlab-nginx-ingress-controller-dwvhm            0/3     Pending     0          23h
svclb-gitlab-nginx-ingress-controller-v6pvd            0/3     Pending     0          23h
gitlab-cainjector-5d5cd7646b-wvvfm                     1/1     Running     0          23h
gitlab-cert-manager-74b6d968d5-59xdq                   1/1     Running     0          23h
gitlab-minio-6dd7d96ddb-s2w9k                          1/1     Running     0          23h
gitlab-minio-create-buckets-1-vqvng                    0/1     Completed   0          23h
gitlab-issuer-1-zf44m                                  0/1     Completed   0          23h
gitlab-registry-5cbc455c95-jrngb                       1/1     Running     0          23h
gitlab-postgresql-0                                    2/2     Running     0          23h
gitlab-registry-5cbc455c95-blv7t                       1/1     Running     0          23h
gitlab-redis-master-0                                  2/2     Running     0          23h
gitlab-prometheus-server-6cfb57f575-p8w9v              2/2     Running     0          23h
gitlab-gitlab-exporter-d69549869-bj85d                 1/1     Running     0          23h
gitlab-gitlab-shell-56698dcffc-mpxf4                   1/1     Running     0          23h
gitlab-gitlab-shell-56698dcffc-9tkqq                   1/1     Running     0          23h
gitlab-task-runner-865ffc85d9-tm2r8                    1/1     Running     0          23h
gitlab-migrations-1-6mgvx                              0/1     Completed   0          23h
gitlab-gitaly-0                                        1/1     Running     0          23h
gitlab-sidekiq-all-in-1-v1-fc8d86c6f-qrjx6             1/1     Running     0          23h
gitlab-webservice-default-77cf99bd45-z6bkb             2/2     Running     0          23h
gitlab-webservice-default-77cf99bd45-9vt5c             2/2     Running     0          23h
gitlab-nginx-ingress-controller-59c4c9bf9b-t8nzw       1/1     Running     0          21h
gitlab-nginx-ingress-controller-59c4c9bf9b-fn2rs       1/1     Running     0          21h
gitlab-nginx-ingress-default-backend-7ff88b95f-4dp5f   1/1     Running     0          23h
...
```



如果您是通过原生k8s安装方式部署，可以通过kubectl命令获取IP，并将该IP关联到之前配置的域名（比如example.com）：[获取实例IP](https://docs.gitlab.com/charts/quickstart/#retrieve-the-ip-address)

如果是通过k3d部署，k3d会通过拉起3个容器作为k8s的运行节点，该节点的端口会通过宿主机的端口映射（6443端口）访问k8s集群，所以只需要将本地的IP关联到example.com即可

```bash
owen@master-node:~$ kubectl get svc/kubernetes -o yaml
apiVersion: v1
kind: Service
metadata:
...
spec:
  clusterIP: 10.43.0.1
  ports:
  - name: https
    port: 443
    protocol: TCP
    targetPort: 6443
...

```

访问部署的实例`https://gitlab.example.com`

![team view](img/gitlab_k8s_inst.png)


